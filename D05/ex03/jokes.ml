let random a = 
	Random.self_init ();
	a.(Random.int (Array.length a))

let () =
	if Array.length Sys.argv > 1
	then
		begin
				let jokeslist = ref [] in
					let ic = open_in (Array.get Sys.argv 1) in
					try
						while true; do
							jokeslist := input_line ic :: !jokeslist
						done;
					with End_of_file ->
						close_in ic;
						print_endline (random (Array.of_list (List.rev !jokeslist)))
		end
