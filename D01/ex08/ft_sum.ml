let ft_sum f min max =
	if max < min 
		then nan
	else
		let rec s_aux f min max acc =
		begin
			if min > max
				then acc
			else
				(s_aux f min (max-1) (acc +. (f max)))
		end
		in s_aux f min max 0.0

let square i = float_of_int (i * i)
let sum i = float_of_int (i + i)
let mul_by_2 i = float_of_int (i * 2)

let () =
	print_float(ft_sum square 1 10);
	print_string ": should be 385.";
	print_newline ();
	print_float(ft_sum sum 1 10);
	print_string ": should be 110.";
	print_newline ();
	print_float(ft_sum mul_by_2 1 10);
	print_string ": should be 110.";
	print_newline ();
	print_float(ft_sum square 10 1);
	print_string ": should be nan.";
	print_newline ()
