let rec repeat_x n =
	if n < 0
	then "Error"
	else if n > 0
		then "x" ^ repeat_x (n -1)
	else ""


let main () =
	print_endline(repeat_x 4);
	print_endline(repeat_x 0);
	print_endline(repeat_x (-1));
	print_endline(repeat_x 1)

(*******************************************)

let () = main ()
