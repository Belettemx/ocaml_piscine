let my_sleep () = Unix.sleep 1

let loop n =
	for i = 0 to n do
		my_sleep ()
	done

let () =
    if Array.length Sys.argv > 1 
	then
		begin
			let secs = try int_of_string (Array.get Sys.argv 1) with 
			| Failure(_) -> 0
			in loop secs
		end
