let is_even n =
    if (n mod 2) = 0
        then true
    else false

let switch_value str n =
    if str.[n] = '1'
        then '0'
    else '1' 

let rec nbr_of_one str len acc i =
    if (i <= len)
    then
        begin
            if ((String.get str i)  ='1')
            then nbr_of_one str len (acc + 1) (i + 1)
            else nbr_of_one str len acc (i + 1)
        end
    else if (acc = 0)
        then (-1)
    else acc

let get_char str i = 
    if (i > 0)
        then (i -1)
    else (-1)

let rec shift n str len count_one=
    if count_one >= 0 && is_even(count_one)
    then
        begin 
            String.set str len (switch_value str len);
            print_string str;
            print_char ' '; 
            shift n str len (nbr_of_one str len 0 0)
        end
    else
        begin
            if (String.contains str '1')
            then
                begin
                    let i_one = String.rindex str '1'
                    in
                    if (i_one = 0)
                       then print_endline ""
                    else if ((get_char str i_one) >= 0)
                    then
                        begin
                            let i_zero = get_char str i_one
                            in
                            String.set str i_zero (switch_value str i_zero);
                            print_string str;
                            print_char ' ';
                            shift n str len (nbr_of_one str len 0 0)
                        end
                    else print_endline ""
                end
             else print_endline ""
         end

let gray n =
    if n <= 0
    then print_newline ()
    else 
        begin
            let str = (String.make n '0')
            in
            print_string str;
            print_char ' ';
            shift n str  (n -1) 0
        end
let () =
    print_endline "gray 0: ";
    gray 0;
    print_endline "gray 1: ";
    gray 1;
    print_endline "gray 3: ";
    gray 3;
    print_endline "gray 4: ";
    gray 4;
    print_endline "gray 5: ";
    gray 5;
    print_endline "gray -1: ";
    gray (-1);
    print_endline "gray 10: ";
    gray 10
