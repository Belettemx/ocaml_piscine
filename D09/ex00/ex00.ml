module Watchtower =
struct	
	type hour = int
	let zero = 12
	let add x y = let x = ((x + y) mod 12) in if x = 0 then 12 else x
	let sub x y = let x = ((x - y) mod 12) in if x < 0 then 12 + x else if x = 0 then 12 else x
end

 let () = 
 	let h: Watchtower.hour = 2 in
 	let h2: Watchtower.hour = 9 in
 	let sum: Watchtower.hour = Watchtower.add h h2 in
 	let sub: Watchtower.hour = Watchtower.sub h h2 in
 	let sub2: Watchtower.hour = Watchtower.sub h Watchtower.zero in
 	let sub3: Watchtower.hour = Watchtower.sub h h in
 	print_endline (string_of_int sum);
 	print_endline (string_of_int sub);
 	print_endline (string_of_int sub2);
 	print_endline (string_of_int sub3);