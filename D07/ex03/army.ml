class ['a] army  =
	object (self)
		val _army:'a list = []

		method add (x:'a) = {<_army = x :: _army>}
		method delete = match _army with
			| (hd)::(tail:'a list) -> {<_army=tail>}
			| [] -> {<_army=_army>}
		method get_first = match _army with
			| (hd:'a)::tl -> hd
			| [] -> failwith "no one there"
	end