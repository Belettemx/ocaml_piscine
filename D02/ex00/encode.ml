let rec count_elem x l count =
	match l with
	| [] -> count
	| head::body ->
		begin
 			if (x = head)
				then count_elem x body (count + 1) 
			else count
		end 

let rec is_in_list x l =
	match l with
	| [] -> false
	| tete::body -> 
		begin
			match tete with
			| (_, y) when y = x -> true
			| (_, y) when y <> x -> false
			| (_, _) -> is_in_list x body
		end

let rec decrypt l acc = 
	match l with
	| [] -> acc
	| head::body ->
		begin 
			if (is_in_list head acc) (* true si elem deja comptabilise   *)
				then decrypt body acc (* poursuite iter de la liste*)
			else
				begin
					decrypt body (((count_elem head body 1), head)::acc) (* add a elem after counting number of elem of it*)
					 (* poursuite iter de la liste*)
				end
		end



let rec encode l =
	decrypt l []

let () =
	let my_print (a, b) = Printf.printf "(%d, %d), " a b in
	let my_print_char (a, b) = Printf.printf "(%d, %c), " a b in
	List.iter my_print (encode (0 :: 0 :: 0 :: 3 :: 2 :: 3 :: 3 :: 3 :: 3 :: 0 :: 0 :: 4 :: []));
	print_newline () ;
	List.iter my_print_char (encode ('a' :: 'a' :: 'a' :: 'b' :: 'c' :: 'b' :: 'b' :: 'b' :: 'b' :: 'a' ::  'a'  :: 'd' :: []));
	print_newline ();
	List.iter my_print (encode []);
	print_newline ()
