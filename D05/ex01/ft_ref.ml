type 'a ft_ref = { mutable inside : 'a }

let return x = { inside = x }

let get ref = ref.inside

let set ref x = ref.inside <- x

let bind ref f : 'b ft_ref = f (get ref)

let () =
    let my_ref = return 12 in
    print_int (get my_ref);
    print_endline "";
    set my_ref 2;
    print_int (get my_ref);
    print_endline "";

	let my_ref_bis = bind my_ref (function  x -> return (x * x)) in 
    print_int (get my_ref_bis);
    print_endline "";
