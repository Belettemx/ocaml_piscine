module StringHash =
	struct
		type t = String.t
		let equal i j = i=j
		let hash i = 
			let rec loop s l = match l with
			| x when x > 0 -> loop (String.get i (x - 1)) (x - 1) lxor int_of_char(s)
			| _ -> 3
		in String.length i lor loop (String.get i ((String.length i) - 1)) ((String.length i) - 1)
	end

module StringHashtbl = Hashtbl.Make(StringHash)

let () =
	let ht = StringHashtbl.create 5 in
	let values = [ "Hello"; "world"; "42"; "Ocaml"; "H" ] in
	let pairs = List.map (fun s -> (s, String.length s)) values in
	List.iter (fun (k,v) -> StringHashtbl.add ht k v) pairs;
	StringHashtbl.iter (fun k v -> Printf.printf "k = \"%s\", v = %d\n" k v) ht