let () =
	let h = new Atom.hydrogen in
	let he = new Atom.helium in
	let li = new Atom.lithium in
	let be = new Atom.berylium in
	let b = new Atom.bore in
	let c = new Atom.carbon in
	let n = new Atom.nitrogen in
	let o = new Atom.oxygen in
	let f = new Atom.fluor in
	let ne = new Atom.neon in
	let na = new Atom.sodium in
	let mg = new Atom.magnesium in
	let al = new Atom.aluminium in
	let si = new Atom.silicium in
	let p = new Atom.phosphorus in
	let s = new Atom.sulfur in
	let cl = new Atom.chlore in
	let ar = new Atom.argon in
	let fe = new Atom.iron in
	let atoms = [h;he;li;be;b;c;n;o;f;ne;na;mg;al;si;p;s;cl;ar;fe] in
	List.iter (fun atom -> Printf.printf "%s -> %d liaison(s)\n" atom#to_string atom#covalent_liaison_nb) atoms;
	print_endline (string_of_bool (h#equals he));
	print_endline (string_of_bool (h#equals c))
