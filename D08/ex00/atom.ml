class virtual atom name symbol atomic_number=
	object (self)
		val _name:string  = name
		val _symbol:string  = symbol
		val _atomic_number:int = atomic_number
		
		method name = _name
		method symbol = _symbol
		method atomic_number = _atomic_number
		method covalent_liaison_nb = if _atomic_number <= 2
									then (2 - _atomic_number)
									else let octet = 8 - ((_atomic_number - 2) mod 8) in
										if octet = 8 then 0 else octet
		method to_string = self#name ^ ", symbol : " ^ self#symbol ^ ", atomic_number: " ^ string_of_int  self#atomic_number
		method equals (that: atom) = self#to_string = that#to_string

	end

class hydrogen =
	object
		inherit atom "hydrogen" "H" 1
	end

class helium =
	object
		inherit atom "helium" "He" 2
	end

class lithium =
	object
		inherit atom "lithium" "Li" 3
	end

class berylium =
	object
		inherit atom "berylium" "Be" 4
	end

class bore =
	object
		inherit atom "bore" "B" 5
	end

class carbon  =
	object
		inherit atom "carbon" "C" 6
	end

class nitrogen  =
object
		inherit atom "nitrogen" "N" 7
end

class oxygen  =
	object
		inherit atom "oxygen"  "O" 8
	end

class fluor  =
	object
		inherit atom "fluor"  "F" 9
	end

class neon  =
	object
		inherit atom "neon"  "Ne" 10
	end

class sodium  =
object
		inherit atom "sodium" "Na" 11
end

class magnesium  =
	object
		inherit atom "magnesium" "Mg" 12
	end

class aluminium  =
	object
		inherit atom "aluminium" "Al" 13
	end

class silicium  =
	object
		inherit atom "silicium" "Si" 14
	end

class phosphorus  =
object
		inherit atom "Phosphorus" "P" 15
end

class sulfur  =
object
		inherit atom "sulfur" "S" 16
end

class chlore  =
object
		inherit atom "chlore" "Cl" 17
end

class argon  =
object
		inherit atom "argon" "Ar" 18
end

class iron =
	object
		inherit atom "iron" "Fe" 26
	end
