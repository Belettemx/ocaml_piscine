class dalek =
	object (self)
		val	_name:string	= 
			begin
				Random.self_init ();
				"Dalek" ^  (String.make 1 (char_of_int ((Random.int 74) + 48))) ^ (String.make 1 (char_of_int ((Random.int 74) + 48))) ^ (String.make 1 (char_of_int ((Random.int 74) + 48)))
			end 
		val _hp:int 		= 100
		val mutable _shield:bool = true
		method to_string = _name ^ ", hp : "  ^ string_of_int (_hp) ^ ", shield : " ^ string_of_bool _shield
		method talk =
			begin
				match Random.int 4 with
				| 0 -> print_endline "Explain! Explain!"
				| 1 -> print_endline "Exterminate! Exterminate!"
				| 2 -> print_endline "I obey!"
				| 3 -> print_endline "You are the Doctor! You are the enemy of the Daleks!"
				| _ -> print_endline "ERRRRRRRRRRRRRRRROOOOOOOOOOORRRRR"
			end
		method switch_shield =
				begin
					if _shield
						then _shield <- false
					else _shield <- true
				end
		method exterminate (p:People.people) = 
			begin
				p#die; 
				self#switch_shield 
			end
		method die = print_endline "Emergency Temporal Shift!"
	end
