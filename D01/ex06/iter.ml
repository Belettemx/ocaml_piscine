let rec iter f x n =
	if n < 0
		then -1
	else if n == 0
		then x
	else
		f(iter f x (n -1))

let () =
	print_int (iter (fun x -> x * x) 2 4);
	print_endline " : should be 65536";
	print_int (iter (fun x -> x * 2) 2 4);
	print_endline " : should be 32";
	print_int (iter (fun x -> x * 0) 2 4);
	print_endline " : should be 0";
