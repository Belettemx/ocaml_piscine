let () =
	let t2 = Value.T2 in 
	let a_s = Value.As in
	let king = Value.King in
	let rec toStringAll ft l = match l with
		| []			-> print_string ""
		| head::tail 	-> 
			begin
				print_endline (ft head);
				toStringAll ft tail
			end
	in
	print_endline "------------  individual --------------------";
	print_endline (Value.toString t2);
	print_endline (Value.toString a_s);
	print_endline "+++";
	print_endline (Value.toStringVerbose t2);
	print_endline (Value.toStringVerbose a_s);
	print_endline "prev king";
	print_endline (Value.toStringVerbose (Value.prev king));
	print_endline "next king";
	print_endline (Value.toStringVerbose (Value.next king));
	print_endline "----------------  all ------------------------";
	toStringAll (Value.toString) Value.all;
print_endline "+++";
toStringAll (Value.toStringVerbose) Value.all;
print_endline "prev a_s";
print_endline (Value.toStringVerbose (Value.prev a_s));
print_endline "To test invalid_arg on next and prev functions uncomment coreesponding lines in main";
print_endline "next a_s";
(*print_endline (Value.toStringVerbose (Value.next a_s)); *)
print_endline "prev T2";

(* print_endline (Value.toStringVerbose (Value.prev t2));*)
print_endline "next T2";
print_endline (Value.toStringVerbose (Value.next t2))
