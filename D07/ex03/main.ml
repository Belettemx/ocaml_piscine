let () =
    let p1 = new People.people "Susan Foreman" in
    let p2 = new People.people "Amelia \"Amy\" Pond" in
    let d1 = new Doctor.doctor "Jon Pertwee" 22 p1 in
    let d2 = new Doctor.doctor "David Tennant" 43 p2 in
    let peoples = new Army.army in
    let newP = (peoples#add p1)#add p2 in
    let doctors = new Army.army in
    let newD = (doctors#add d1)#add d2 in
    print_endline (newP#get_first)#to_string;
    print_endline ((newP#delete)#get_first)#to_string;
    print_endline (newD#get_first)#to_string;
    print_endline ((newD#delete)#get_first)#to_string;
    let dalek1 = new Dalek.dalek in
    let dalek2 = new Dalek.dalek in
    let daleks = new Army.army in
    let newDaleks = (daleks#add dalek1)#add dalek2 in
    print_endline (newDaleks#get_first)#to_string;
    print_endline ((newDaleks#delete)#get_first)#to_string;