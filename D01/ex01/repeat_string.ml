let rec repeat_string ?(str ="x") n =
	if n < 0
	then "Error"
	else if n > 0
		then (repeat_string ~str:str (n -1)) ^ str
	else ""


let main () =
	print_endline(repeat_string ~str:"x" 0);
	print_endline(repeat_string ~str:"hahaha" (-1));
	print_endline(repeat_string ~str:"toto" 1);
	print_endline(repeat_string ~str:"Toto" 3);
	print_endline(repeat_string 4)

(*******************************************)
let () = main ()
