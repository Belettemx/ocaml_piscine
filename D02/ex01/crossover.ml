let rec comp x l_b = 
	match l_b with
	| [] -> false
	| head::body -> (head = x) || (comp x body)

let rec decrypt l_a l_b acc =
	match l_a with
	| [] -> acc
	| head::body ->
		begin
			if (comp head l_b)
				then decrypt body l_b (head::acc)
			else
				decrypt body l_b acc
		end


let crossover l_a l_b =
	if l_a = [] || l_b = []
		then []
	else decrypt l_a l_b []

let n = 1 :: 2 :: 3 :: 1 :: 2 :: 3 :: 7 :: 8 :: 9 :: 10 ::[]
let n1 = 1 :: 3 :: 7 :: 8 :: 9 :: 10 ::[]
let n2 = 4 :: 5 :: 6 :: []

let m1 = 'a' :: 'b' :: 'c' :: []
let m2 = 'c' :: []

let () =
	let my_print x = Printf.printf "%d, " x in
	let my_print_char x = Printf.printf "%c " x in
	List.iter my_print (crossover n n1);
	print_endline ": should display 10, 9, 8, 7, 3, 1, 3, 1";
	List.iter my_print (crossover n n2);
	print_endline ": should display none";
	List.iter my_print_char (crossover m1 m2);
	print_endline ": should display c";
	List.iter my_print (crossover [] n2);
	print_endline ": should display none";
	List.iter my_print (crossover n2 []);
	print_endline ": should display none";

