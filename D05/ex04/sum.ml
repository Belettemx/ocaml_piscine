let sum = ( +. )

let () =
	print_float (sum 4. 5.);
	print_endline "";
	print_float (sum 0. 5.);
	print_endline "";
	print_float (sum 4. (-5.));
	print_endline "";
	print_float (sum 4. 38.42);
	print_endline "";
	print_float (sum 4.1 5.05);
	print_endline "";
	print_float (sum 4.4 5.4);
	print_endline "";
	print_float (sum 27.4 15.02);
	print_endline ""
