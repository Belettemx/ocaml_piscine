module Color = struct
	type t = Spade | Heart | Diamond | Club

	let all = [Spade; Heart; Diamond; Club]

	let toString t = match t with
		| Spade -> "S"
		| Heart -> "H"
		| Diamond -> "D"
		| Club -> "C"

	let toStringVerbose t = match t with
		| Spade -> "Spade"
		| Heart -> "Heart"
		| Diamond -> "Diamond"
		| Club -> "Club"

end

module Value = struct
	type t = T2 | T3 | T4 | T5 | T6 | T7 | T8 | T9 | T10 | Jack | Queen | King | As

	let all = [T2; T3; T4; T5; T6; T7; T8; T9; T10; Jack; Queen; King; As]

	let toInt t = match t with
			| T2 	-> 	1
			| T3 	-> 	2
			| T4 	-> 	3
			| T5 	-> 	4
			| T6 	-> 	5
			| T7 	-> 	6
			| T8 	-> 	7
			| T9 	-> 	8
			| T10 	-> 	9
			| Jack 	-> 	10
			| Queen -> 	11
			| King 	-> 	12
			| As 	-> 	13

	let toString t = match t with
			| T2 	-> 	"2"
			| T3 	-> 	"3"
			| T4 	-> 	"4"
			| T5 	-> 	"5"
			| T6 	-> 	"6"
			| T7 	-> 	"7"
			| T8 	-> 	"8"
			| T9 	-> 	"9"
			| T10 	-> 	"10"
			| Jack 	-> 	"J"
			| Queen -> 	"Q"
			| King 	-> 	"K"
			| As 	-> 	"A"

	let toStringVerbose t = match t with
			| T2 	-> 	"2"
			| T3 	-> 	"3"
			| T4 	-> 	"4"
			| T5 	-> 	"5"
			| T6 	-> 	"6"
			| T7 	-> 	"7"
			| T8 	-> 	"8"
			| T9 	-> 	"9"
			| T10 	-> 	"10"
			| Jack 	-> 	"Jack"
			| Queen -> 	"Queen"
			| King 	-> 	"King"
			| As 	-> 	"As"

	let next t = match t with
			| T2 	-> 	T3
			| T3 	-> 	T4
			| T4 	-> 	T5
			| T5 	-> 	T6
			| T6 	-> 	T7
			| T7 	-> 	T8
			| T8 	-> 	T9
			| T9 	-> 	T10
			| T10 	-> 	Jack
			| Jack 	-> 	Queen
			| Queen -> 	King
			| King 	-> 	As
			| As 	-> 	invalid_arg "no next"

	let previous  t = match t with
			| T2 	-> 	invalid_arg "no previous"
			| T3 	-> 	T2
			| T4 	-> 	T3
			| T5 	-> 	T4
			| T6 	-> 	T5
			| T7 	-> 	T6
			| T8 	-> 	T7
			| T9 	-> 	T8
			| T10 	-> 	T9
			| Jack 	-> 	T10
			| Queen -> 	Jack
			| King 	-> 	Queen
			| As 	-> 	King

end

type t = {
	color : Color.t;
	value : Value.t
}

let newCard v c =  {color=c; value=v}

let allSpades  = [	{color=Color.Spade; value=Value.T2};
					{color=Color.Spade; value=Value.T3};
					{color=Color.Spade; value=Value.T4};
					{color=Color.Spade; value=Value.T5};
					{color=Color.Spade; value=Value.T6};
					{color=Color.Spade; value=Value.T7};
					{color=Color.Spade; value=Value.T8};
					{color=Color.Spade; value=Value.T9};
					{color=Color.Spade; value=Value.T10};
					{color=Color.Spade; value=Value.Jack};
					{color=Color.Spade; value=Value.Queen};
					{color=Color.Spade; value=Value.King};
					{color=Color.Spade; value=Value.As}]

let allHearts  = [	{color=Color.Heart; value=Value.T2};
					{color=Color.Heart; value=Value.T3};
					{color=Color.Heart; value=Value.T4};
					{color=Color.Heart; value=Value.T5};
					{color=Color.Heart; value=Value.T6};
					{color=Color.Heart; value=Value.T7};
					{color=Color.Heart; value=Value.T8};
					{color=Color.Heart; value=Value.T9};
					{color=Color.Heart; value=Value.T10};
					{color=Color.Heart; value=Value.Jack};
					{color=Color.Heart; value=Value.Queen};
					{color=Color.Heart; value=Value.King};
					{color=Color.Heart; value=Value.As}]

let allDiamonds  = [{color=Color.Diamond; value=Value.T2};
					{color=Color.Diamond; value=Value.T3};
					{color=Color.Diamond; value=Value.T4};
					{color=Color.Diamond; value=Value.T5};
					{color=Color.Diamond; value=Value.T6};
					{color=Color.Diamond; value=Value.T7};
					{color=Color.Diamond; value=Value.T8};
					{color=Color.Diamond; value=Value.T9};
					{color=Color.Diamond; value=Value.T10};
					{color=Color.Diamond; value=Value.Jack};
					{color=Color.Diamond; value=Value.Queen};
					{color=Color.Diamond; value=Value.King};
					{color=Color.Diamond; value=Value.As}]

let allClubs  = [	{color=Color.Club; value=Value.T2};
					{color=Color.Club; value=Value.T3};
					{color=Color.Club; value=Value.T4};
					{color=Color.Club; value=Value.T5};
					{color=Color.Club; value=Value.T6};
					{color=Color.Club; value=Value.T7};
					{color=Color.Club; value=Value.T8};
					{color=Color.Club; value=Value.T9};
					{color=Color.Club; value=Value.T10};
					{color=Color.Club; value=Value.Jack};
					{color=Color.Club; value=Value.Queen};
					{color=Color.Club; value=Value.King};
					{color=Color.Club; value=Value.As}]

let all  =	 [		{color=Color.Spade; value=Value.T2};
					{color=Color.Spade; value=Value.T3};
					{color=Color.Spade; value=Value.T4};
					{color=Color.Spade; value=Value.T5};
					{color=Color.Spade; value=Value.T6};
					{color=Color.Spade; value=Value.T7};
					{color=Color.Spade; value=Value.T8};
					{color=Color.Spade; value=Value.T9};
					{color=Color.Spade; value=Value.T10};
					{color=Color.Spade; value=Value.Jack};
					{color=Color.Spade; value=Value.Queen};
					{color=Color.Spade; value=Value.King};
					{color=Color.Spade; value=Value.As};
					{color=Color.Heart; value=Value.T2};
					{color=Color.Heart; value=Value.T3};
					{color=Color.Heart; value=Value.T4};
					{color=Color.Heart; value=Value.T5};
					{color=Color.Heart; value=Value.T6};
					{color=Color.Heart; value=Value.T7};
					{color=Color.Heart; value=Value.T8};
					{color=Color.Heart; value=Value.T9};
					{color=Color.Heart; value=Value.T10};
					{color=Color.Heart; value=Value.Jack};
					{color=Color.Heart; value=Value.Queen};
					{color=Color.Heart; value=Value.King};
					{color=Color.Heart; value=Value.As};
					{color=Color.Diamond; value=Value.T2};
					{color=Color.Diamond; value=Value.T3};
					{color=Color.Diamond; value=Value.T4};
					{color=Color.Diamond; value=Value.T5};
					{color=Color.Diamond; value=Value.T6};
					{color=Color.Diamond; value=Value.T7};
					{color=Color.Diamond; value=Value.T8};
					{color=Color.Diamond; value=Value.T9};
					{color=Color.Diamond; value=Value.T10};
					{color=Color.Diamond; value=Value.Jack};
					{color=Color.Diamond; value=Value.Queen};
					{color=Color.Diamond; value=Value.King};
					{color=Color.Diamond; value=Value.As};
					{color=Color.Club; value=Value.T2};
					{color=Color.Club; value=Value.T3};
					{color=Color.Club; value=Value.T4};
					{color=Color.Club; value=Value.T5};
					{color=Color.Club; value=Value.T6};
					{color=Color.Club; value=Value.T7};
					{color=Color.Club; value=Value.T8};
					{color=Color.Club; value=Value.T9};
					{color=Color.Club; value=Value.T10};
					{color=Color.Club; value=Value.Jack};
					{color=Color.Club; value=Value.Queen};
					{color=Color.Club; value=Value.King};
					{color=Color.Club; value=Value.As}]

let getValue t = match t with
	| {color=_;value=v} -> v

let getColor t = match t with
	| {color=c;value=_} -> c

let toString t = 
	Value.toString t.value ^ Color.toString t.color

let toStringVerbose t = 
	Value.toStringVerbose t.value ^ " of " ^ Color.toStringVerbose t.color

let compare t t_bis = 
	(Value.toInt t.value) - (Value.toInt t_bis.value)
	
let max t t_bis =
	if ((Value.toInt t.value) - (Value.toInt t_bis.value) >= 0)
		then t
	else t_bis

let min t t_bis =
	if ((Value.toInt t.value) - (Value.toInt t_bis.value) <= 0)
		then t
	else t_bis

let best l = match l with
	| [] -> invalid_arg "no cards"
	| head::tail -> List.fold_left max head tail

let isOf t color =	t.color = color

let isSpade t =	t.color = Color.Spade 

let isHeart t = t.color = Color.Heart 

let isDiamond t = t.color = Color.Diamond 

let isClub t = t.color = Color.Club 
