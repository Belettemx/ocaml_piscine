type 'a tree = Nil | Node of 'a * 'a tree * 'a tree

let size bt = 
	let rec loop n bt = match bt with
	| Node (_, l, r) -> loop (loop (n + 1) l) r
	| Nil -> n
	in loop 0 bt


let length bt = 
	let rec loop n bt = match bt with
	| Node (_, l, r) -> begin
			let lenl = loop (n + 1) l 
			and lenr = loop (n + 1) r
			in if lenl > lenr
			then lenl
			else lenr
		end
	| Nil -> n
	in loop 0 bt

let draw_square x y size str =
	Graphics.moveto x y;
	let x1 = (x - (size / 2))
	in
	let y1 = (y - (size / 2))
	in  
		begin
			Graphics.moveto x1 y1;
			Graphics.lineto (x1 + size) y1 ;
			Graphics.lineto (x1 + size) (y1 + size);
			Graphics.lineto x1 (y1 + size);
			Graphics.lineto x1 y1;
			Graphics.moveto (x - (String.length str) / 2 * 8) (y - 5);
			Graphics.draw_string str;
		end

let draw_tree bt =
	let rec loop x y bt = match bt with
		| Nil -> draw_square x y 50 "Nil";
		| Node (value, l, r) ->
			begin
				draw_square 			x 					y 			50 value;
				Graphics.moveto 		(x + 25) 			y;
				Graphics.lineto 		(x + 2 * 50 - 25) 	(y + (50 * length bt));
				loop 					(x + 2 * 50) 		(y + (50 * length bt)) 	l;
				Graphics.moveto 		(x + 25) 			y;
				Graphics.lineto 		(x + 2 * 50 - 25) 	(y - (50 * length bt));
				loop 					(x + 2 * 50) 		(y - (50 * length bt)) 	r;
			end
		in loop 100 250 bt



(* ************************************************** *)

let main () = 
	let leafl = Node ("leafl", 	Nil, 	Nil)
	and leafr = Node ("leafr", 	Nil, 	Nil)
	in
	let root1 = Node ("root1",	Nil, 	Nil )
	and root2 = Node ("root2",	leafl, 	Nil )
	and root3 = Node ("root3",	Nil, 	leafr )
	and root4 = Node ("root4",	leafl, 	leafr )
	in
	print_endline "=== Node Size test ===";
	print_int (size root1) ; print_newline ();
	print_int (size root2) ; print_newline ();
	print_int (size root3) ; print_newline ();
	print_int (size root4) ; print_newline ();
	print_endline "=== Node length test ===";
	print_int (length root1) ; print_newline ();
	print_int (length root2) ; print_newline ();
	print_int (length root3) ; print_newline ();
	print_int (length root4) ; print_newline ();

	print_endline "=== Node length test ===";
	Graphics.open_graph " 500x500";
	draw_tree root1;
	ignore(Graphics.read_key ());
	Graphics.open_graph " 500x500";
	draw_tree root3;
	ignore(Graphics.read_key ());
	Graphics.open_graph " 500x500";
	draw_tree root4;
	ignore(Graphics.read_key ())

let () = main ();;